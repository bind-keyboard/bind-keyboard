export const singleQuote = true;
export const trailingComma = "all";
export const printWidth = 120;
export const tabWidth = 2;