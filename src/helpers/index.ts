// @index('./*', f => `export { default as ${f.name} } from "${f.path}";`)
export { default as getKeyСombination } from "./getKeyСombination";
export { default as isInputOrTextArea } from "./isInputOrTextArea";
export { default as keyParser } from "./keyParser";
// @endindex
